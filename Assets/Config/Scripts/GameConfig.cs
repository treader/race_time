﻿using UnityEngine;
using UnityEditor;

public class GameConfig : ScriptableObject
{
    public int lapCount = 3;
}

#if UNITY_EDITOR
public class CreateGameDataConfig
{
    [MenuItem("Treader/Create/GameDataConfig")]
    public static void CreateMyAsset()
    {
        GameConfig asset = ScriptableObject.CreateInstance<GameConfig>();

        AssetDatabase.CreateAsset(asset, "Assets/Config/ScriptableObjects/GameDataConfig.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
#endif