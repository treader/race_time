﻿using System;
using UnityEngine;

public class ServiceLocator : MonoBehaviour
{
    static ServiceLocator mInstance;

    public static ServiceLocator pInstance
    {
        get
        {
            return mInstance;
        }
    }

    [SerializeField] GameConfig gameConfig;
    [SerializeField] ScoreKeeper scoreKeeper;
    [SerializeField] CheckPointListener checkPointListener;

    void Awake()
    {
        mInstance = this;
    }

    public GameConfig GetConfig()
    {
        if (gameConfig != null)
        {
            return gameConfig;
        }

        return new GameConfig();
    }
    
    public ScoreKeeper GetScoreKeeper()
    {
        if (scoreKeeper != null)
        {
            return scoreKeeper;
        }

        return new ScoreKeeper();
    }

    public IObservable GetCheckPointListenerObservable()
    {
        if (checkPointListener != null)
        {
            return checkPointListener;
        }

        return new CheckPointListener();
    }
}
