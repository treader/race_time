﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Treader
{
    public abstract class StateBase : MonoBehaviour
    {
        public string stateName;

        // Use this for initialization
        public virtual void Init()
        {

        }

        public virtual void DeInit()
        {

        }
    }
}
