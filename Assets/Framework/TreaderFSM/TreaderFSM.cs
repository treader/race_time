﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Treader
{
    public class TreaderFSM : MonoBehaviour
    {
        [SerializeField] List<StateBase> stateBases;

        private static TreaderFSM instance;
        private StateBase currentState;

        public static TreaderFSM Instance
        {
            get
            {
                return instance;
            }

            private set
            {
                instance = value;
            }
        }

        public void Awake()
        {
            instance = this;
        }

        public void ChangeState(string stateName)
        {
            for (int i = 0; i < stateBases.Count; i++)
            {
                if (stateBases[i].stateName.Equals(stateName))
                {
                    if (currentState != null)
                    {
                        currentState.DeInit();
                    }

                    currentState = stateBases[i];
                    currentState.Init();
                }
            }
        }
    }
}
