﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class States
{
    public const string MAIN_MENU_STATE = "MainMenu";
    public const string GAME_MODE_LAP = "GameModeLap";
    public const string GAME_END_STATE = "GameEndState";
    public const string GAME_INFO_STATE = "GameInfoState";
}
