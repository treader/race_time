﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treader;
using System;

public class GameModeLapLogic : StateBase
{
    [SerializeField] GameObject raceTrack;
    [SerializeField] CarInput carInput;
    [SerializeField] Car car;
    [SerializeField] UIInGameHUD inGameHUD;
    [SerializeField] UIPreStart preStart;

    [SerializeField] AudioSource ignitionSFX;

    public override void Init()
    {
        base.Init();
        raceTrack.SetActive(true);
        car.Init(carInput);
        ServiceLocator.pInstance.GetScoreKeeper().Init();
        inGameHUD.Init();
        InitPreStart();
    }

    public override void DeInit()
    {
        base.DeInit();
        car.DeInit();
        ServiceLocator.pInstance.GetScoreKeeper().DeInit();
        inGameHUD.DeInit();
    }

    private void InitPreStart()
    {
        preStart.Init();
        preStart.OnPreStartDeInit += HandlePreStartDeInit;
        carInput.inputEnabled = false;
    }

    private void HandlePreStartDeInit()
    {
        carInput.inputEnabled = true;
        ignitionSFX.Play();
        preStart.OnPreStartDeInit -= HandlePreStartDeInit;
    }
}
