﻿using UnityEngine;
using Treader;
using System;

public class ScoreKeeper : MonoBehaviour, IObserver
{
    public Action<ScoreKeeper> OnGameOver;
    public Action<int> OnLapUpdated;
    public Action<float> OnRunningTimeUpdate;

    IObservable checkPointListener;

    int lapsCount = 0;
    bool crossedMid = false;
    
    float lapStartTime = 0;

    static float endTime = 0;
    static float bestTime = 0;

    bool startTimer = false;

    float currentRunningTimeMilliSeconds = 0;
    float tempRunningTime = 0;

    public float pEndTime
    {
        get
        {
            return endTime;
        }
    }

    public float pBestTime
    {
        get
        {
            return bestTime;
        }
    }

    public void Init()
    {
        InitObservable();
        lapsCount = 0;
        bestTime = 0;
        lapStartTime = 0;
        currentRunningTimeMilliSeconds = 0;
    }

    public void DeInit()
    {
        startTimer = false;
        checkPointListener.Remove(this);
    }
    
    public void ObservableUpdate(object obj)
    {
        bool isStart = (bool)obj;

        if (isStart)
        {
            HandleStartCrossed();
        }
        else
        {
            HandleMidCrossed();
        }
    }

    public void BeginTimer()
    {
        startTimer = true;

        lapStartTime = currentRunningTimeMilliSeconds - lapStartTime;
    }

    private void HandleMidCrossed()
    {
        crossedMid = true;
    }

    private void HandleStartCrossed()
    {
        if (crossedMid)
        {
            lapsCount++;
            crossedMid = false;
            UpdateBestTime();

            if(OnLapUpdated != null)
            {
                OnLapUpdated(lapsCount);
            }

            if (lapsCount == ServiceLocator.pInstance.GetConfig().lapCount)
            {
                endTime = currentRunningTimeMilliSeconds;
                TreaderFSM.Instance.ChangeState(States.GAME_END_STATE);
            }
        }
    }

    void UpdateBestTime()
    {
        float lapTime;
        lapTime = currentRunningTimeMilliSeconds - lapStartTime;

        if (lapTime < bestTime || bestTime == 0)
        {
            bestTime = lapTime;
        }
        lapStartTime = lapTime;
    }

    void InitObservable()
    {
        checkPointListener = ServiceLocator.pInstance.GetCheckPointListenerObservable();
        checkPointListener.Add(this);
    }

    void Update()
    {
        if (startTimer)
        {
            tempRunningTime += Time.deltaTime;
            if (tempRunningTime >= 0.1)
            {
                currentRunningTimeMilliSeconds += tempRunningTime;
                tempRunningTime = 0;

                if (OnRunningTimeUpdate != null)
                {
                    OnRunningTimeUpdate(currentRunningTimeMilliSeconds);
                }
            }
        }
    }
}
