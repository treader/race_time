﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointListener : MonoBehaviour, IObservable
{

    [SerializeField] Transform startLine;
    [SerializeField] Transform midLine;

    List<IObserver> observers;
    bool startCrossed;

    public void Add(IObserver observer)
    {
        if (observer != null)
        {
            observers.Add(observer);
        }
    }

    public void Remove(IObserver observer)
    {
        if (observer != null)
        {
            observers.Remove(observer);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.Equals(startLine))
        {
            startCrossed = true;
            Notify();
        }

        if (collision.transform.Equals(midLine))
        {
            startCrossed = false;
            Notify();
        }
    }

    public void Notify()
    {
        for (int i = 0; i < observers.Count; i++)
        {
            observers[i].ObservableUpdate(startCrossed);
        }
    }

    private void Awake()
    {
        observers = new List<IObserver>();
    }
}
