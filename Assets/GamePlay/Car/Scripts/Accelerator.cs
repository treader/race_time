﻿using UnityEngine;

public class Accelerator
{
    public float maxSpeed;
    Transform transform;
    float accelerationStep;

    public float velocity = 0;

    public Accelerator(Transform transform, float maxSpeed, float accelerationStep)
    {
        this.transform = transform;
        this.maxSpeed = maxSpeed;
        this.accelerationStep = accelerationStep;
    }

    public float Accelerate()
    {
        if (velocity < maxSpeed)
        {
            velocity += accelerationStep;
        }
        else
        {
            velocity -= accelerationStep;
        }

        transform.Translate(transform.up * velocity * Time.deltaTime, Space.World);

        return velocity;
    }
}
