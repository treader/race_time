﻿using System;
using UnityEngine;

public class Car : MonoBehaviour
{
    float velocity = 0;

    [SerializeField] CarData carData;
    [SerializeField] Transform initialMarker;

    CarInput carInput;
    Accelerator accelerator;
    Steering steering;
    Reverse reverse;
    SmoothStop smoothStop;

    public void Init(CarInput carInput)
    {
        this.carInput = carInput;
        SubscribeEvents();
        accelerator = new Accelerator(transform, carData.maxSpeed, carData.accelerationStep);
        steering = new Steering(transform, carData.turnSpeed);
        reverse = new Reverse(transform, carData.maxReverseSpeed, carData.decelerationStep);
        smoothStop = new SmoothStop();
        transform.position = initialMarker.position;
        transform.rotation = initialMarker.rotation;
    }

    public void DeInit()
    {
        velocity = 0;
        UnSubscribeEvents();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        accelerator.maxSpeed = carData.maxSpeedWhileColliding;
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        accelerator.maxSpeed = carData.maxSpeed;
    }

    void SubscribeEvents()
    {
        carInput.OnAccelerate += HandleOnCarAccelerate;
        carInput.OnIdle += HandleOnCarIdle;
        carInput.OnTurnLeft += HandleOnCarTurnLeft;
        carInput.OnTurnRight += HandleOnCarTurnRight;
        carInput.OnReverse += HandleOnReverse;
        carInput.OnReverseBegin += HandleOnReverseBegin;
        carInput.OnAccelerateBegin += OnAccelerateBegin;
    }

    void UnSubscribeEvents()
    {
        carInput.OnAccelerate -= HandleOnCarAccelerate;
        carInput.OnIdle -= HandleOnCarIdle;
        carInput.OnTurnLeft -= HandleOnCarTurnLeft;
        carInput.OnTurnRight -= HandleOnCarTurnRight;
        carInput.OnReverse -= HandleOnReverse;
        carInput.OnReverseBegin -= HandleOnReverseBegin;
        carInput.OnAccelerateBegin -= OnAccelerateBegin;
    }

    private void OnAccelerateBegin()
    {
        accelerator.velocity = velocity;
    }

    private void HandleOnReverseBegin()
    {
        reverse.velocity = velocity;
    }

    private void HandleOnReverse()
    {
        velocity = reverse.Decelerate();
    }

    private void HandleOnCarTurnRight()
    {
        if(velocity > carData.tunrnThershold)
            steering.Turn(Steering.Direction.Right);
    }

    private void HandleOnCarTurnLeft()
    {
        if(velocity > carData.tunrnThershold)
            steering.Turn(Steering.Direction.Left);
    }

    private void HandleOnCarIdle()
    {
        velocity = smoothStop.Stop(velocity, carData.decelerationStep);

        if (velocity != 0)
            transform.Translate(transform.up * velocity * Time.deltaTime, Space.World);
    }

    private void HandleOnCarAccelerate()
    {
        velocity = accelerator.Accelerate();
    }
}
