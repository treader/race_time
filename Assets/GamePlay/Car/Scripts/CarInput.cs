﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CarInput : MonoBehaviour
{
    public Action OnAccelerate;
    public Action OnAccelerateBegin;
    public Action OnReverse;
    public Action OnReverseBegin;
    public Action OnTurnRight;
    public Action OnTurnLeft;
    public Action OnIdle;

    public bool inputEnabled = false;

    // Update is called once per frame
    void Update ()
    {
        if (inputEnabled)
        {
            HandleInput();
        }
	}

    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (OnReverseBegin != null)
                OnReverseBegin();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (OnAccelerateBegin != null)
                OnAccelerateBegin();
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (OnTurnRight != null)
                OnTurnRight();
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (OnTurnLeft != null)
                OnTurnLeft();
        }

        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow))
        {
            if (OnAccelerate != null)
                OnAccelerate();
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            if (OnReverse != null)
                OnReverse();
        }
        else
        {
            if (OnIdle != null)
                OnIdle();
        }
    }
}
