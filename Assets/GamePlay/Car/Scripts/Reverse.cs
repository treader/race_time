﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reverse
{
    Transform transform;
    float maxSpeed;
    float decelerationStep;
    
    public float velocity;

    public Reverse(Transform transform, float maxSpeed, float decelerationStep)
    {
        this.transform = transform;
        this.maxSpeed = maxSpeed;
        this.decelerationStep = decelerationStep;
    }

    public float Decelerate ()
    {
        velocity -= decelerationStep;

        if (velocity < maxSpeed)
            velocity = maxSpeed;

        transform.Translate(transform.up * velocity * Time.deltaTime, Space.World);

        return velocity;
    }
}
