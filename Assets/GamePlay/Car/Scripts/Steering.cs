﻿using UnityEngine;

public class Steering
{
    public enum Direction
    {
        Right = -1,
        Left = 1
    }

    Transform transform;
    float turnSpeed;

    public Steering(Transform transform, float turnSpeed)
    {
        this.transform = transform;
        this.turnSpeed = turnSpeed;
    }

    public void Turn(Direction direction)
    {
        transform.Rotate(0, 0, (int)direction * turnSpeed * Time.deltaTime);
    }
}
