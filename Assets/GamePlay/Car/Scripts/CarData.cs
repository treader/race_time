﻿using UnityEngine;
using UnityEditor;

public class CarData : ScriptableObject
{
    public float maxSpeed = 5;
    public float maxSpeedWhileColliding = 2.5f;
    public float accelerationStep = 0.2f;
    public float maxReverseSpeed = -2;
    public float decelerationStep = 0.08f;
    public float turnSpeed = 50f;
    public float tunrnThershold = 2;
}

#if UNITY_EDITOR
public class CreateCarData
{
    [MenuItem("Treader/Create/CarData")]
    public static void CreateMyAsset()
    {
        CarData asset = ScriptableObject.CreateInstance<CarData>();

        AssetDatabase.CreateAsset(asset, "Assets/GamePlay/Car/ScriptableObjects/CarData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
#endif