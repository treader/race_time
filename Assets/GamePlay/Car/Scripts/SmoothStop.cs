﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothStop
{
    public float Stop(float velocity, float decelerationStep)
    {
        if (velocity > 0)
        {
            velocity -= decelerationStep;
            if (velocity < 0)
                velocity = 0;
        }
        else
        {
            velocity += decelerationStep;
            if (velocity > 0)
                velocity = 0;
        }

        return velocity;
    }
}
