﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treader;

public class UIMainMenu : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        if (TreaderFSM.Instance != null)
            TreaderFSM.Instance.ChangeState(States.MAIN_MENU_STATE);
	}

    public void OnClickStart()
    {
        TreaderFSM.Instance.ChangeState(States.GAME_MODE_LAP);
    }

    public void OnClickInfo()
    {
        TreaderFSM.Instance.ChangeState(States.GAME_INFO_STATE);
    }

    public void Init()
    {
        gameObject.SetActive(true);
    }

    public void DeInit()
    {
        gameObject.SetActive(false);
    }
}
