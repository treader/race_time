﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treader;

public class MainMenuLogic : StateBase
{
    [SerializeField] UIMainMenu uIMainMenu;
    public override void Init()
    {
        base.Init();
        uIMainMenu.Init();
    }

    public override void DeInit()
    {
        base.DeInit();
        uIMainMenu.DeInit();
    }
}
