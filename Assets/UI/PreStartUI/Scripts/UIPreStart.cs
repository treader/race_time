﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class UIPreStart : MonoBehaviour
{
    public Action OnPreStartDeInit;

    [SerializeField] float blinkTime = 0.2f;
    [SerializeField] float totalTime = 3;

    [SerializeField] string[] textToShow;
    [SerializeField] Text lableReadySetGo;


    float actualShowTime = 0;
    float currentShowTime = 0;
    float currentBlinkTime = 0;

    bool startPreStart = false;
    bool isShowing = true;
    int currentIndex = 0;

    public void Init()
    {
        gameObject.SetActive(true);
        CalculateActualShowTime();
        startPreStart = true;
        isShowing = true;
        currentShowTime = 0;
        currentIndex = 0;
        UpdateTextByIndex(currentIndex);
    }

    public void DeInit()
    {
        startPreStart = false;
        gameObject.SetActive(false);
        ServiceLocator.pInstance.GetScoreKeeper().BeginTimer();

        if (OnPreStartDeInit != null)
        {
            OnPreStartDeInit();
        }
    }

    void CalculateActualShowTime()
    {
        if (textToShow != null && textToShow.Length > 0)
        {
            actualShowTime = totalTime/textToShow.Length - blinkTime;
        }
    }

    void Update()
    {
        if (startPreStart)
        {
            UpdateShowTime();
            UpdateBlinkTime();
        }
    }

    void UpdateTextByIndex(int index)
    {
        if (textToShow != null && textToShow.Length > index)
        {
            lableReadySetGo.gameObject.SetActive(true);
            lableReadySetGo.text = textToShow[index];
            isShowing = true;
        }
        else
        {
            DeInit();
        }
    }

    void UpdateShowTime()
    {
        if (isShowing && currentShowTime < actualShowTime)
        {
            currentShowTime += Time.deltaTime;
        }
        else if (currentBlinkTime == 0)
        {
            currentShowTime = 0;
            isShowing = false;
        }
    }

    void UpdateBlinkTime()
    {
        if (!isShowing && currentBlinkTime < blinkTime)
        {
            currentBlinkTime += Time.deltaTime;
            lableReadySetGo.gameObject.SetActive(false);
        }
        else if (currentShowTime == 0)
        {
            isShowing = true;
            currentBlinkTime = 0;
            currentIndex++;
            UpdateTextByIndex(currentIndex);
        }
    }
}
