﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treader;

public class GameOverStateLogic : StateBase
{
    [SerializeField] UIEndScreen uiEndScreen;

    public override void Init()
    {
        base.Init();
        uiEndScreen.Init();
    }

    public override void DeInit()
    {
        base.DeInit();
        uiEndScreen.DeInit();
    }
}
