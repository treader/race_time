﻿using UnityEngine;
using Treader;
using UnityEngine.UI;
using Treader.Utils;
public class UIEndScreen : MonoBehaviour
{
    [SerializeField] Text totalTime;
    [SerializeField] Text bestTime;

    ScoreKeeper scoreKeeper;
    public void Init()
    {
        scoreKeeper = ServiceLocator.pInstance.GetScoreKeeper();
        gameObject.SetActive(true);
        UpdateTimer();
    }

    public void DeInit()
    {
        gameObject.SetActive(false);
    }

    public void OnClickPlay()
    {
        TreaderFSM.Instance.ChangeState(States.GAME_MODE_LAP);
    }

    void UpdateTimer()
    {
        totalTime.text = FormatText.FormatTimeMilliSeconds(scoreKeeper.pEndTime);

        bestTime.text = FormatText.FormatTimeMilliSeconds(scoreKeeper.pBestTime);
    }
}
