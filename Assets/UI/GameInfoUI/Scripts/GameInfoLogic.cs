﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treader;

public class GameInfoLogic : StateBase
{
    [SerializeField] UIGameInfo uIGameInfo;

    public override void Init()
    {
        base.Init();
        uIGameInfo.Init();
    }

    public override void DeInit()
    {
        base.DeInit();
        uIGameInfo.DeInit();
    }
}
