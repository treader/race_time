﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treader;

public class UIGameInfo : MonoBehaviour
{
    public void Init()
    {
        gameObject.SetActive(true);
    }

    public void DeInit()
    {
        gameObject.SetActive(false);
    }

    public void OnMenuClick()
    {
        TreaderFSM.Instance.ChangeState(States.MAIN_MENU_STATE);
    }

    public void OnStartGameClick()
    {
        TreaderFSM.Instance.ChangeState(States.GAME_MODE_LAP);
    }
}
