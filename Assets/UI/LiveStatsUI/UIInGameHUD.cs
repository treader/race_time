﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Treader.Utils;

public class UIInGameHUD : MonoBehaviour
{
    [SerializeField] Text lapValue;
    [SerializeField] Text bestTime;
    [SerializeField] Text runningTotalTimer;
 
    ScoreKeeper scoreKeeper;
    GameConfig gameConfig;

    public void Init()
    {
        InitServiceLocatorObjects();
        gameObject.SetActive(true);
        SubscribeEvents();
        InitTimerValues();
    }

    public void DeInit()
    {
        gameObject.SetActive(false);
        UnSubscribeEvents();
    }

    void SubscribeEvents()
    {
        scoreKeeper.OnLapUpdated += HandleLapUpdated;
        scoreKeeper.OnRunningTimeUpdate += HandleRunningTimeUpdate;
    }

    private void HandleRunningTimeUpdate(float runningTime)
    {
        runningTotalTimer.text = FormatText.FormatTimeMilliSeconds(runningTime);
    }

    private void HandleLapUpdated(int lapCount)
    {
       lapValue.text = lapCount +"/"+gameConfig.lapCount;
       bestTime.text = FormatText.FormatTimeMilliSeconds(scoreKeeper.pBestTime);
    }

    void UnSubscribeEvents()
    {
        scoreKeeper.OnLapUpdated -= HandleLapUpdated;
        scoreKeeper.OnRunningTimeUpdate -= HandleRunningTimeUpdate;
    }

    void InitServiceLocatorObjects()
    {
        scoreKeeper = ServiceLocator.pInstance.GetScoreKeeper();
        gameConfig = ServiceLocator.pInstance.GetConfig();
    }

    void InitTimerValues()
    {
        lapValue.text = 0 + "/" + gameConfig.lapCount;
        bestTime.text = "00:00:00";
        runningTotalTimer.text = "00:00:00";
    }
}
